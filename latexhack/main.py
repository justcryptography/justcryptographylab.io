#!/usr/bin/env python3

with open('testrun') as test:
    with open('template.tex') as temp:
        temp = temp.read()
        for line in test.readlines():
            line = line.strip()
            with open('output/%s.tex' % (line,), 'w') as out:
                out.write(temp.replace('%s', line))
