#!/usr/bin/env python3
import sys


def main():
    words = sys.argv[3:]
    with open(sys.argv[1]) as template:
        svg = template.read()
        for i in range(0,5):
            svg = svg.replace('{text%d}' % (i+1,), words[i])
            
    with open(sys.argv[2], 'w') as target:
        target.write(svg)
            

if __name__ == '__main__':
    main()
