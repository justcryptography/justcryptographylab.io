#!/usr/bin/env python3
import os
import sys
from pprint import pprint
from collections import Counter

# python 3.12
# from itertools import batched
# else
def batched(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

blocked = set(['however', 'hence'])


def sanitize(word):
    return word.strip(',:().').lower()


def read_file(fname):
    with open(fname) as fd:
        sanitized = [ sanitize(word) for word in fd.read().split() ]
        return [ word for word in sanitized if len(word) > 3 and word not in blocked ]

def main():
    content = Counter()

    global blocked
    blocked |= set(read_file('1000.txt'))

    for fname in sys.argv[1:]:
        content.update(read_file(fname))

    lines = batched([word[0] for word in content.most_common(2000)], 10)
    lines = list(lines)
    
    # for line in lines:
    #     for word in line:
    #         sys.stdout.write("{0:18}".format(word))
    #     print("")

    for i in range(0,10):
        with open(f'wordlist{i}.txt', 'w') as outfile:
            for line in lines:
                outfile.write(f'0 {line[i]}\n')



if __name__ == '__main__':
    main()
